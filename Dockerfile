ARG IMAGE="golang:1.21.13-bookworm"
ARG CROSSBUILD=false

FROM ${IMAGE}

ENV DEBIAN_FRONTEND=noninteractive
RUN set -xe \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
    build-essential \
    libarchive-tools \
    ca-certificates \
    curl \
    rclone \
    git \
    gnupg \
    make \
    msitools \
    wixl \
    openssh-client \
    unzip \
    uuid-runtime \
    && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/trusted.gpg.d/docker.gpg \
    && echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/trusted.gpg.d/docker.gpg] https://download.docker.com/linux/debian \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
    docker-ce-cli \
    docker-buildx-plugin \
# Install base BSD for CROSS
    && if ${CROSSBUILD}; then \
    for OS in freebsd openbsd; do \
        for ARCH in amd64 arm64; do \
            DIR="/sysroot/${OS}/${ARCH}"; \
            mkdir -p "${DIR}"; \
            case "${OS}" in \
                freebsd) \
                    URL="https://download.freebsd.org/releases/${ARCH}/14.2-RELEASE/base.txz"; \
                    TAR_FLAGS="-xJf"; \
                    FILE="base.txz"; \
                    ;; \
                openbsd) \
                    URL="https://cdn.openbsd.org/pub/OpenBSD/7.6/${ARCH}/base76.tgz"; \
                    TAR_FLAGS="-xzf"; \
                    FILE="base76.tgz"; \
                    ;; \
            esac; \
            wget -q -O "${DIR}/${FILE}" "${URL}"; \
            bsdtar ${TAR_FLAGS} "${DIR}/${FILE}" -C "${DIR}"; \
            rm "${DIR}/${FILE}"; \
        done; \
    done; \
    fi \
# Remove trash
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install ffmpeg
ARG FF_VER="7.0"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && URL="https://releases.yucca.app/ffmpeg/${FF_VER}" \
    && FF_ARCHIVE="ffmpeg-${FF_VER}-linux-amd64.tar.gz" \
    && cd "${TMPDIR}" \
    && wget -q ${URL}/${FF_ARCHIVE} -O "${FF_ARCHIVE}" \
    && wget -q ${URL}/${FF_ARCHIVE}.sha256sum -O "${FF_ARCHIVE}.sha256sum" \
    && sha256sum --ignore-missing -c "${FF_ARCHIVE}.sha256sum" \
    && tar -xzf "${FF_ARCHIVE}" \
    && mv ffmpeg /usr/local/bin/ \
    && mv ffprobe /usr/local/bin/ \
    && rm -rf "${TMPDIR}"

# Install goreleaser
ARG GORELEASER_VER="v2.5.1"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && URL="https://github.com/goreleaser/goreleaser-pro/releases/download/${GORELEASER_VER}-pro/goreleaser-pro_Linux_x86_64.tar.gz" \
    && cd "${TMPDIR}" \
    && wget -q ${URL} \
    && tar -xzf goreleaser-pro_Linux_x86_64.tar.gz \
    && mv goreleaser /usr/local/bin/ \
    && rm -rf "${TMPDIR}"

# Install cosign
ARG COSIGN_VER="v2.4.1"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && cd "${TMPDIR}" \
    && URL="https://github.com/sigstore/cosign/releases/download/${COSIGN_VER}/cosign-linux-amd64" \
    && wget -q ${URL} \
    && mv cosign-linux-amd64 /usr/local/bin/cosign \
    && chmod +x /usr/local/bin/cosign \
    && rm -rf "${TMPDIR}"

# Install syft
ARG SYFT_VER="v1.17.0"
RUN curl -sSfL https://raw.githubusercontent.com/anchore/syft/${SYFT_VER}/install.sh | sh -s -- -b /usr/local/bin

# Install quill
ARG QUILL_VER="0.5.0"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && cd "${TMPDIR}" \
    && URL="https://github.com/anchore/quill/releases/download/v${QUILL_VER}/quill_${QUILL_VER}_linux_amd64.tar.gz" \
    && wget -q ${URL} \
    && tar -xzf "quill_${QUILL_VER}_linux_amd64.tar.gz" \
    && mv quill /usr/local/bin/ \
    && chmod +x /usr/local/bin/quill \
    && quill --version \
    && rm -rf "${TMPDIR}"

# Install hashicorp vault
ARG VAULT_VER="1.18.3"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && URL="https://releases.hashicorp.com/vault/${VAULT_VER}/vault_${VAULT_VER}_linux_amd64.zip" \
    && cd "${TMPDIR}" \
    && wget -q ${URL} \
    && unzip "vault_${VAULT_VER}_linux_amd64.zip" \
    && chmod +x vault \
    && mv vault /usr/local/bin/ \
    && vault version \
    && rm -rf "${TMPDIR}"

# Install releasectl
ARG RELEASECTL_VER="2.6.12"
RUN set -xe \
    && TMPDIR=$(mktemp -d) \
    && URL="https://tools.s-ed1.cloud.gcore.lu/releasectl/${RELEASECTL_VER}/releasectl_${RELEASECTL_VER}_linux_amd64.tar.gz" \
    && cd "${TMPDIR}" \
    && wget -q ${URL} \
    && tar -xzf "releasectl_${RELEASECTL_VER}_linux_amd64.tar.gz" \
    && chmod +x releasectl \
    && mv releasectl /usr/local/bin/ \
    && releasectl --help \
    && rm -rf "${TMPDIR}"

ENTRYPOINT [ "/bin/bash" ]
