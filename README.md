# Golang docker image for cross compile to macos, linux, windows, freebsd, openbsd

Образ с `C,C++` компиляторами, для кросс компиляции golang c CGO_ENABLED=1 из linux, под macOS, linux(arm,arm64), windows, freebsd, openbsd.  
Сборка пакетов под Darwin на основе проекта [osxcross](https://github.com/tpoechtrager/osxcross).  
~~Предварительно нужно скачать Xcode c сайта [Apple](https://developer.apple.com/download/more/). Текущая версия Xcode_14.3.1.xip, её нужно положить в корень проекта.~~ Ранее мы собирали это сами но сейчас базовый образ на основе [goreleaser-cross](https://github.com/goreleaser/goreleaser-cross), примеры есть также у них [тут](https://github.com/goreleaser/example-cross/tree/master).

## Пример сборки под MacOS

### amd64

```bash
GOOS=darwin \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC=o64-clang \
CXX=o64-clang++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### arm64

```bash
GOOS=darwin \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC=oa64-clang \
CXX=oa64-clang++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

## Пример сборки под Windows

### amd64

```bash
GOOS=windows \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC=x86_64-w64-mingw32-gcc \
CXX=x86_64-w64-mingw32-g++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

## Пример сборки под linux

### amd64

```bash
GOOS=linux \
GOARCH=arm \
CGO_ENABLED=1 \
CC=x86_64-linux-gnu-gcc \
CXX=x86_64-linux-gnu-g++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### arm (v7)

```bash
GOOS=linux \
GOARCH=arm \
CGO_ENABLED=1 \
CC=arm-linux-gnueabihf-gcc \
CXX=arm-linux-gnueabihf-g++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### arm64

```bash
GOOS=linux \
GOARCH=arm64 \
CGO_ENABLED=1 \
CC=aarch64-linux-gnu-gcc \
CXX=aarch64-linux-gnu-g++ \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### Пример c cтатической линковкой

```bash
GOOS=linux \
GOARCH=arm \
CGO_ENABLED=1 \
CC=arm-linux-gnueabihf-gcc \
CXX=arm-linux-gnueabihf-g++ \
CGO_LDFLAGS="-static"
go build -ldflags "-linkmode external -extldflags=\"${CGO_LDFLAGS}\"" -o build/${GOOS}_${GOARCH}/example ./main.go
```

## Пример сборки под FreeBSD

### amd64

```bash
GOOS=freebsd \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC="clang --target=x86_64-unknown-freebsd --sysroot=/sysroot/freebsd/amd64" \
CXX="clang++ --target=x86_64-unknown-freebsd --sysroot=/sysroot/freebsd/amd64" \
CGO_CFLAGS="--sysroot=/sysroot/freebsd/amd64" \
CGO_LDFLAGS="--sysroot=/sysroot/freebsd/amd64 -L/sysroot/freebsd/amd64/lib" \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### arm64

```bash
GOOS=freebsd \
GOARCH=arm64 \
CGO_ENABLED=1 \
CC="clang --target=aarch64-unknown-freebsd --sysroot=/sysroot/freebsd/arm64" \
CXX="clang++ --target=aarch64-unknown-freebsd --sysroot=/sysroot/freebsd/arm64" \
CGO_CFLAGS="--sysroot=/sysroot/freebsd/arm64" \
CGO_LDFLAGS="--sysroot=/sysroot/freebsd/arm64 -L/sysroot/freebsd/arm64/lib" \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### Пример c cтатической линковкой

```bash
GOOS=freebsd \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC="clang --target=x86_64-unknown-freebsd --sysroot=/sysroot/freebsd/amd64" \
CXX="clang++ --target=x86_64-unknown-freebsd --sysroot=/sysroot/freebsd/amd64" \
CGO_CFLAGS="-static --sysroot=/sysroot/freebsd/amd64" \
CGO_LDFLAGS="-static --sysroot=/sysroot/freebsd/amd64 -L/sysroot/freebsd/amd64/lib" \
go build -ldflags "-linkmode external -extldflags=\"${CGO_LDFLAGS}\"" -o build/${GOOS}_${GOARCH}/example ./main.go
```

## Пример сборки под OpenBSD

### amd64

```bash
GOOS=openbsd \
GOARCH=amd64 \
CGO_ENABLED=1 \
CC="clang --target=x86_64-unknown-openbsd --sysroot=/sysroot/openbsd/amd64" \
CXX="clang++ --target=x86_64-unknown-openbsd --sysroot=/sysroot/openbsd/amd64" \
CGO_CFLAGS="--sysroot=/sysroot/openbsd/amd64" \
CGO_LDFLAGS="--sysroot=/sysroot/openbsd/amd64 -L/sysroot/openbsd/amd64/lib" \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```

### arm64

```bash
GOOS=openbsd \
GOARCH=arm64 \
CGO_ENABLED=1 \
CC="clang --target=aarch64-unknown-openbsd --sysroot=/sysroot/openbsd/arm64" \
CXX="clang++ --target=aarch64-unknown-openbsd --sysroot=/sysroot/openbsd/arm64" \
CGO_CFLAGS="--sysroot=/sysroot/openbsd/arm64" \
CGO_LDFLAGS="--sysroot=/sysroot/openbsd/arm64 -L/sysroot/openbsd/arm64/lib" \
go build -o build/${GOOS}_${GOARCH}/example ./main.go
```
