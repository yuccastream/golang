#!/bin/sh

set -eu

export REVISION=${REVISION:-'r8'}

export BASE_IMAGE=${BASE_IMAGE:-'golang'}
export BASE_IMAGE_TAG=${BASE_IMAGE_TAG:-'1.24.0-bookworm'}

if [ "${BASE_IMAGE}" != "golang" ] ; then
    export VER_TAG="${BASE_IMAGE_TAG}-cross-${REVISION}"
    export CROSSBUILD=true
else
    export VER_TAG="${BASE_IMAGE_TAG}-${REVISION}"
    export CROSSBUILD=false
fi

export COSIGN_VER=${COSIGN_VER:-'v2.4.1'}
export FF_VER=${FF_VER:-'6.0'}
export GORELEASER_VER=${GORELEASER_VER:-'v2.7.0'}
export QUILL_VER=${QUILL_VER:-'0.5.0'}
export RELEASECTL_VER=${RELEASECTL_VER:-'2.6.12'}
export SYFT_VER=${SYFT_VER:-'v1.17.0'}
export VAULT_VER=${VAULT_VER:-'1.18.3'}

main() {

    TAG=${VER_TAG}
    IMAGE="${BASE_IMAGE}:${BASE_IMAGE_TAG}"

    echo "Build registry.gitlab.com/yuccastream/golang:${TAG}"
    docker build \
        --build-arg IMAGE=${IMAGE} \
        --build-arg CROSSBUILD=${CROSSBUILD} \
        --build-arg COSIGN_VER=${COSIGN_VER} \
        --build-arg FF_VER=${FF_VER} \
        --build-arg GORELEASER_VER=${GORELEASER_VER} \
        --build-arg QUILL_VER=${QUILL_VER} \
        --build-arg RELEASECTL_VER=${RELEASECTL_VER} \
        --build-arg SYFT_VER=${SYFT_VER} \
        --build-arg VAULT_VER=${VAULT_VER} \
        -t registry.gitlab.com/yuccastream/golang:${TAG} \
        -f ${PWD}/Dockerfile ${PWD}
    docker push registry.gitlab.com/yuccastream/golang:${TAG}
}

main "$@"
